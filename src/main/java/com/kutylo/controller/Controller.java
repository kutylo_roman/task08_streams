package com.kutylo.controller;

import com.kutylo.model.task1.NumbersInterface;
import com.kutylo.model.task2.Command;
import com.kutylo.model.task2.CommandImpl;
import com.kutylo.model.task4.Task4;

import java.util.ArrayList;

public class Controller {

    //Task1
    public int getMaxNumber(int num1, int num2, int num3){
        NumbersInterface numbersInterface=(a, b, c)->{
            if((a > b)&&(a > c)) return a;
                else if((b > a)&&(b > c)) return b;
                    else return c;
        };
        return numbersInterface.calculate(num1, num2, num3);
    }

    public int getAverageOfNumbers(int num1, int num2, int num3){
        NumbersInterface numbersInterface=(int a, int b, int c)->((a+b+c)/3);
        return numbersInterface.calculate(num1, num2, num3);
    }

    //Task2
    ArrayList<Command> commands=new ArrayList<>();

    public void initialListCommands(){
        commands.add(new Command() {
            @Override
            public String getString(String string) {
                return "Anonymous class";
            }
        });

        commands.add(s->"Lambda"+"s");
        commands.add(this::getString);
        commands.add(new CommandImpl());
    }

    private String getString(String s) {
        return "Called as method reference. Your message: " + s;
    }

    public String executeCommand(String name, String arg) {
        String result = null;
        if (name.equals("anonymous")) {
            result = commands.get(0).getString(arg);
        } else if (name.equals("lambda")) {
            result = commands.get(1).getString(arg);
        } else if (name.equals("reference")) {
            result = commands.get(2).getString(arg);
        } else if (name.equals("object")) {
            result = commands.get(3).getString(arg);
        }
        return result;
    }

    //Task4
    Task4 lines;

    public void initLines(String str) {
        lines = new Task4(str);
    }

    public Task4 getLines() {
        return lines;
    }

}
