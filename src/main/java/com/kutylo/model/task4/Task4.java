package com.kutylo.model.task4;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task4 {
    String lines;
    Stream<String> stringStream;

    public Task4(String lines) {
        this.lines = lines;
        stringStream = Stream.of(lines);
    }

    public int getUniqueWordsCount() {
        return (int) stringStream.distinct().count();
    }

    public List<String> getSortedUniqueWords() {
        return stringStream.distinct().collect(Collectors.toList());
    }

    public Map<String, Long> getWordStatistic() {
        return stringStream.flatMap(e -> Stream.of(e.split(" "))).collect(Collectors.groupingBy(e -> e, Collectors.counting()));
    }

    public Map<String, Long> getSymbolStatistic() {
        return stringStream.flatMap(e -> Stream.of(e.split(""))).collect(Collectors.groupingBy(e -> e, Collectors.counting()));
    }
}
