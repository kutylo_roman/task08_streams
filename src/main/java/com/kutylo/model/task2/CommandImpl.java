package com.kutylo.model.task2;


public class CommandImpl implements Command {
    @Override
    public String getString(String string){
        return "Called as object of command class.";
    }
}
