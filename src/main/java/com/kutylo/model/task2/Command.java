package com.kutylo.model.task2;

@FunctionalInterface
public interface Command {
    String getString(String string);
}
