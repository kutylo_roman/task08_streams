package com.kutylo.model.task1;

@FunctionalInterface
public interface NumbersInterface {
    int calculate(int num1,int num2, int num3);
}
