package com.kutylo.model.task3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.Stream;

public class Task3 {
    private ArrayList<Integer> taskList=new ArrayList<>();
    private Random random;

    private void initialList(int limit){
        for(int i=0;i<limit;i++){
            taskList.add(random.nextInt());
        }
    }

    public void toStream() {
        initialList(20);
        Stream<Integer> stream = taskList.stream();
        Stream<Integer> stream1 = Stream.iterate(2, t -> (t + 10));
        Stream<Integer> stream2 = Stream.of(5, 6, 3, 9);
    }


    public Map<String,Integer> getStreamInfo(Stream<Integer> stream) {
        Map<String, Integer> map = new HashMap<>();
        map.put("min", stream.min(Integer::compareTo).get());
        map.put("max", stream.max(Integer::compareTo).get());

        map.put("average", (calculateSum(stream)/ (int)(stream.count())));
        map.put("sum", new Integer(calculateSum(stream)));
        map.put("large", (int)stream.filter((s)->(s>map.get("average"))).count());
        return map;
    }

    private Integer calculateSum(Stream<Integer> stream){
        Integer sumValues = stream.reduce((sum, elem) -> (sum += elem)).get();
        return sumValues;
    }
}
